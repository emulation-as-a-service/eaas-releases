# Release-Channel Metadata Example
This repository contains example metadata for Emulation-as-a-Service's release-channels.

## Adding new release-channels
To add a new release-channel, run the following command:
```sh
$ make channel name=<channel-name>
```

## Adding new releases
To add a new release to an existing channel, first add a new release definition file located at
`./sources/<channel>/XY` with content formatted as follows:
```
# artifact definitions
A|<id-1>|<kind-1>|<location-1>
...
A|<id-n>|<kind-n>|<location-n>

# release definition
R|<version>|<id-1>,<id-2>,...,<id-n>
```

Artifact IDs `<id-n>` can be arbitrary integers, but they must be unique locally to each file.
Multiple releases can also be defined per file, referencing subsets of previously defined artifacts.
Use `R!` (instead of `R`) to add a release and mark it as latest.

For example, a release `1.0` with 2 artifacts can be defined like this:
```
# artifact definitions
A|1|server|https://gitlab.com/emulation-as-a-service/eaas-server/-/jobs/763882969/artifacts/raw/eaas-server.ear
A|2|ui|https://gitlab.com/emulation-as-a-service/demo-ui/-/jobs/733020359/artifacts/download

# release
R|1.0|1,2
```

After new releases are added, update channel's metadata by running the following command:
```sh
$ make <channel-name>
```

When all checks pass successfully, commit updated metadata and push repository to the remote server.
