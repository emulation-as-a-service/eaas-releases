srcdir = sources
outdir = channels

generator = ./tools/metadata-generator
credentials = ./credentials.json


.ONESHELL:  # run all commands in a single shell!

$(credentials):
	# create an empty credentials file
	echo '[]' > $(credentials)

channel: $(credentials)
	# initialize new channel-metadata
	$(generator) -f $(outdir)/$(name).json initialize $(name)
	mkdir -p $(srcdir)/$(name)

$(outdir)/%.json: $(srcdir)/%/*
	# apply batch-updates to specified channel
	$(generator) -f $@ -c $(credentials) apply $^

%: $(outdir)/%.json
	# check channel-metadata consistency
	$(generator) -f $< check
